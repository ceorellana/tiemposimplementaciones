import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class FrequencyCounter {

    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<String> archivos = new ArrayList();
        archivos.add("tale.txt");
        archivos.add("tinyTale.txt");
        archivos.add("leipzig1M.txt");
        Scanner reader = new Scanner(System.in);  // Reading from System.in
        System.out.println("Ingrese el número mínimo de letras: ");
        int minlen = reader.nextInt(); // Scans the next token of the input as an int.
        System.out.println("1. tinyTale\n2. tale\n3. leipzig1M\n Escoja el archivo a analizar: ");
        int op = reader.nextInt();
        while(op <0 || op >4){
            System.out.println("Escoja una opcion valida: ");
            op = reader.nextInt();
        }
        reader.close();

        System.out.println("Implementacion Hash Table");
        long startTime = System.nanoTime();
        SymbolTable<Integer> st = new SymbolTable();
        Scanner in = new Scanner(new File(archivos.get(op)));
        while(in.hasNextLine()) // Checks if there is another line
        {
            Scanner check = new Scanner(in.nextLine());
            while(check.hasNext()){
                String word = check.next();
                if (word.length() < minlen) continue; // Ignore short keys.
                if (!st.contains(word)) st.put(word, 1);
                else st.put(word, st.get(word) + 1);
            }
            check.close();
        }
        in.close();
        // Find a key with the highest frequency count.
        String max = "";
        st.put(max, 0);
        for (String word : st.keys())
            if (st.get(word) > st.get(max))
                max = word;
        long endTime = System.nanoTime();
        long duration = (endTime - startTime)/1000000;
        System.out.println(max + " " + st.get(max));
        System.out.println("Tiempo de ejecucion: " + duration + " milisegundos");

        System.out.println("Implementacion Arbol Binario");
        startTime = System.nanoTime();
        BinarySearchTree<String,Integer> st2 = new BinarySearchTree<>();
        Scanner in1 = new Scanner(new File(archivos.get(op)));
        while(in1.hasNextLine()) // Checks if there is another line
        {
            Scanner check1 = new Scanner(in1.nextLine());
            while(check1.hasNext()){
                String word = check1.next();
                if (word.length() < minlen) continue; // Ignore short keys.
                if (!st2.contains(word)) st2.put(word, 1);
                else st2.put(word, st2.get(word) + 1);
            }
            check1.close();
        }
        in1.close();
        // Find a key with the highest frequency count.
        max = "";
        st2.put(max, 0);
        for (String word : st2.keys())
            if (st2.get(word) > st2.get(max))
                max = word;
        endTime = System.nanoTime();
        duration = (endTime - startTime)/1000000;
        System.out.println(max + " " + st2.get(max));
        System.out.println("Tiempo de ejecucion: " + duration + " milisegundos");

        System.out.println("Implementacion Arboles Rojo-Negro");
        startTime = System.nanoTime();
        BinarySearchTree<String,Integer> st3 = new BinarySearchTree<>();
        Scanner in2 = new Scanner(new File(archivos.get(op)));
        while(in2.hasNextLine()) // Checks if there is another line
        {
            Scanner check3 = new Scanner(in2.nextLine());
            while(check3.hasNext()){
                String word = check3.next();
                if (word.length() < minlen) continue; // Ignore short keys.
                if (!st3.contains(word)) st3.put(word, 1);
                else st3.put(word, st3.get(word) + 1);
            }
            check3.close();
        }
        in2.close();
        // Find a key with the highest frequency count.
        max = "";
        st3.put(max, 0);
        for (String word : st3.keys())
            if (st3.get(word) > st3.get(max))
                max = word;
        endTime = System.nanoTime();
        duration = (endTime - startTime)/1000000;
        System.out.println(max + " " + st3.get(max));
        System.out.println("Tiempo de ejecucion: " + duration + " milisegundos");
    }
}