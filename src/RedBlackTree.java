import java.util.TreeMap;

public class RedBlackTree <Key extends Comparable<Key>, Value>{

        private TreeMap<Key, Value> st = new TreeMap<Key, Value>();
        public void put(Key key, Value val)
        {
            if (val == null) st.remove(key);
            else st.put(key, val);
        }
        public Value get(Key key) { return st.get(key); }
        public Value remove(Key key) { return st.remove(key); }
        public boolean contains(Key key) { return st.containsKey(key); }
        public Iterable<Key> keys() { return st.keySet(); }
}
